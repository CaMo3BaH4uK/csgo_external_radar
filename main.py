import pymem
import pymem.process
import time
import sys
import os
import multiprocessing	
import overlay
import win32api
import win32con
from PySide2 import QtWidgets, QtCore, QtGui

def ParseMap(map):
	pos_x = float(0)
	pos_y = float(0)
	scale = float(0)
	rotate = float(0)
	zoom = float(0)
	mapFile = open(os.path.dirname(sys.argv[0]) + "/maps/"+ map +".txt", "r")
	mapFileLines = mapFile.readlines()
	for line in mapFileLines:
		try:
			lineData = float(line.split('"')[3])
			if "pos_x" in line:
				pos_x = lineData
			elif "pos_y" in line:
				pos_y = lineData
			elif "scale" in line:
				scale = lineData
			elif "rotate" in line:
				rotate = lineData
			elif "zoom" in line:
				zoom = lineData
		except:
			pass
	if(pos_x and pos_y and scale):
		return pos_x, pos_y, scale, rotate, zoom
	else:
		print("FATAL ERROR. ParseMap failed.")
		quit()


#engine
dwClientState = (0x58ADD4)
dwClientState_ViewAngles = (0x4D88)
dwClientState_GetLocalPlayer = (0x180)
dwClientState_Map = (0x28C)

#panorama
dwLocalPlayer = (0xD3ABEC)
m_bHasHelmet = (0xB36C)
m_iHealth = (0x100)
m_ArmorValue = (0xB378)
m_vecOrigin = (0x138)
m_iTeamNum = (0xF4)
m_hMyWeapons = (0x2DF8)
m_iItemDefinitionIndex = (0x2FAA)
dwEntityList = (0x4D4F25C)
dwNextPlayer = (0x10)
m_iCrosshairId = (0xB3E4)



#init
pym = pymem.Pymem("csgo.exe")

engine = pymem.process.module_from_name(pym.process_handle, "engine.dll").lpBaseOfDll
panorama = pymem.process.module_from_name(pym.process_handle, "client.dll").lpBaseOfDll
print("Engine Address: " + hex(engine))
engine_ClientState = pym.read_int(engine + dwClientState)
print("dwClientState address: " + hex(engine_ClientState))

print("\nPANORAMA")
localPlayer = pym.read_int(panorama + dwLocalPlayer)
print("Panorama localPlayer address " + hex(localPlayer))
entityList = pym.read_int(panorama + dwEntityList)

currentMap = str(pym.read_string(engine_ClientState + dwClientState_Map))
pos_x, pos_y, scale, rotate, zoom = ParseMap(currentMap)

#custom variables
players = []
trigger = False
radar = False
radarOffset = [10, 66]
radarScaleX = 4.4
radarScaleY = 4.4


class CrosshairThread(QtCore.QThread):
	signal = QtCore.Signal()
	def __init__(self):
		QtCore.QThread.__init__(self)

	def run(self):
		global trigger
		while True:
			if not trigger and win32api.GetAsyncKeyState(win32con.VK_END):
				trigger = True
				time.sleep(0.2)
			if trigger:
				crosshair = pym.read_int(localPlayer + m_iCrosshairId)
				crosshairEntity = pym.read_uint(panorama + dwEntityList + ((crosshair - 1) * dwNextPlayer))
				crosshairEntityTeam = pym.read_int(crosshairEntity + m_iTeamNum)
				crosshairEntityHealth = pym.read_int(crosshairEntity + m_iHealth) 
				if(crosshairEntityTeam > 0 and crosshairEntityHealth > 0 and pym.read_int(crosshairEntity + m_iTeamNum) != pym.read_int(localPlayer + m_iTeamNum)):
					x,y = win32api.GetCursorPos()
					win32api.mouse_event(win32con.MOUSEEVENTF_LEFTDOWN,x,y,0,0)
					time.sleep(0.05)
					win32api.mouse_event(win32con.MOUSEEVENTF_LEFTUP,x,y,0,0)
					time.sleep(0.18)
				if win32api.GetAsyncKeyState(win32con.VK_END):
					trigger = False
					time.sleep(0.2)


class RadarThread(QtCore.QThread):
	signal = QtCore.Signal()
	def __init__(self):
		QtCore.QThread.__init__(self)

	def run(self):
		global players, radar, pos_x, pos_y, scale, radarScaleX, radarScaleY, radarOffset
		while True:
			if win32api.GetAsyncKeyState(win32con.VK_TAB):
				radar = True
				players.clear()
				for i in range(0,32):
					entity = pym.read_int(panorama + dwEntityList + i*dwNextPlayer)
					if entity != 0 and pym.read_int(entity + m_iHealth) > 0 and pym.read_int(entity + m_iTeamNum) != pym.read_int(localPlayer + m_iTeamNum): # and pym.read_int(entity + m_iTeamNum) != pym.read_int(localPlayer + m_iTeamNum)
						#dot_color = 'yellow'
						for j in range(0,17):
							try:
								weapon = pym.read_int(entity + m_hMyWeapons + j)
								if weapon > 0:
									weapon_base = pym.read_int(entity + m_hMyWeapons + (j - 1) * 0x4) & 0xFFF
									weapon_entity = pym.read_int(panorama + dwEntityList + (weapon_base - 1) * 0x10)
									weapon_id = pym.read_int(weapon_entity + m_iItemDefinitionIndex)
									if weapon_id == 49:
										pass
										#dot_color = 'blue'
							except:
								pass
						""" print("Health: " + str(pym.read_int(entity + m_iHealth)))
						print("Armor: " + str(pym.read_int(entity + m_ArmorValue)))
						print("X: " + str(pym.read_float(entity + m_vecOrigin)))
						print("Y: " + str(pym.read_float(entity + m_vecOrigin + 0x4)))
						print("Z: " + str(pym.read_float(entity + m_vecOrigin + 0x8)))
						print("\nNext") """
						playerX = pym.read_float(entity + m_vecOrigin) - pos_x
						playerY = pym.read_float(entity + m_vecOrigin + 0x4) - pos_y
						playerX /= 3.65
						playerY /= -3.65
						playerX /= radarScaleX
						playerY /= radarScaleY
						playerX += radarOffset[0]
						playerY += radarOffset[1]
						players.append([playerX,playerY])
				print(players)
				self.signal.emit()
				time.sleep(0.2)
			else:
				radar = False
				players.clear()
				self.signal.emit()

class MainApp(QtWidgets.QMainWindow, overlay.Ui_MainWindow):
	def updatetext(self):
		self.label.setText('<html><head/><body><p><span style=" color:#aaff00;" size="5">TR '+ str(trigger) + '</span></p></body></html>')
		self.update()

	def __init__(self):
		super().__init__()
		self.setupUi(self)
		self.updatetext()
		self.radarWorker = RadarThread()
		self.radarWorker.signal.connect(self.updatetext)
		self.radarWorker.start()
		self.crosshairWorker = CrosshairThread()
		self.crosshairWorker.start()

	def paintEvent(self, e):
		qp = QtGui.QPainter()
		qp.begin(self)
		self.drawGeometry(qp)
		qp.end()

	def drawGeometry(self, qp):
		if radar:
			qpoly = QtGui.QPolygonF( [QtCore.QPointF(p[0], p[1]) for p in players] )
			qp.setPen(QtGui.QPen(QtCore.Qt.green, 4, QtCore.Qt.DashLine))
			qp.drawPoints(qpoly)
		

if __name__ == "__main__":
	app = QtWidgets.QApplication(sys.argv)
	window = MainApp()
	window.show()
	app.exec_()